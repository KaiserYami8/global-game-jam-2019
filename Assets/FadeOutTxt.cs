﻿using UnityEngine;

public class FadeOutTxt : MonoBehaviour {

	void Update () {
        transform.GetComponent<CanvasGroup>().alpha -= Time.deltaTime / 3;
        if (transform.GetComponent<CanvasGroup>().alpha <= 0)
        {
            Destroy(gameObject);
        }
    }
}