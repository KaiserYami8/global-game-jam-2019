﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lancement_MJ : MonoBehaviour {

    [SerializeField]
    GameObject Jeu;

	public void JeuActive()
    {
        Jeu.SetActive(true);
        FindObjectOfType<PlayerController>().jeu = true;
    }
}
