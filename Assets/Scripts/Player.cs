﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    [SerializeField]
    Camera cam;

    [SerializeField]
    public GameObject Target;

    public NavMeshAgent agent;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {

        //if (Input.GetMouseButtonDown(0))
        //{
        //    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;

        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        Debug.Log(new Vector3(hit.point.x, hit.point.y, transform.position.z));
        //        agent.SetDestination(new Vector3(hit.point.x, hit.point.y, hit.point.z));
        //    }
        //}

        if(Input.GetKeyDown(KeyCode.T))
        {
            GoToTarget();
        }

    }

    public void GoToTarget()
    {
        agent.SetDestination(Target.transform.position);
    }
}