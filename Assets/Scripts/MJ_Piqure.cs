﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MJ_Piqure : MonoBehaviour {

    [SerializeField]
    KeyCode Desactivate;

    [SerializeField]
    float speed;

    [SerializeField]
    bool Planter = false;

    [SerializeField]
    GameObject Target;

    [SerializeField]
    GameObject Starter;

    [SerializeField]
    GameObject Piqure;

    private void Start()
    {
        Starter = GameObject.FindGameObjectWithTag("Start");
        Target = GameObject.FindGameObjectWithTag("Target");
    }

    void Update ()
    {
		if(Input.GetKeyDown(Desactivate))
        {
            GetComponent<MJ_SinMovement>().enabled = false;
            Planter = true;
        }

        if(Planter)
        {
            transform.position += transform.right * -speed * Time.deltaTime;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Piqure")
        {
            Planter = false;
            if (collision.gameObject == Target)
            {
                Debug.Log("<color=red> WIN </color>");
            }
            else if (collision.tag == "bras")
            {
                Starter.GetComponent<MJ_NePik>().NewPiqure();
                Destroy(this);
            }
        }
    }
}
