﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MJ_Linge : MonoBehaviour {

    [SerializeField]
    GameObject ObjPorter;

    [SerializeField]
    bool Porter = false;

    private void Start()
    {
        FindObjectOfType<MJ_MachineAlaver>().enabled = true;
    }

    public void Update()
    {
        if (Porter)
        {
            if (Input.GetButton("Fire1"))
            {
                ObjPorter.transform.position = transform.position;
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            Porter = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Linge")
        {
            Porter = true;
            ObjPorter = collision.gameObject;
        }
    }
}
