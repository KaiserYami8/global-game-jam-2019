﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MJ_MachineAlaver : MonoBehaviour {

    [SerializeField]
    List<GameObject> Linge;

    [SerializeField]
    int count;

    [SerializeField]
    Toggle list;

    // Use this for initialization
    void Start () {

        foreach(GameObject GO in GameObject.FindGameObjectsWithTag("Linge"))
        {
            Linge.Add(GO);
        }
	}

    private void Update()
    {
        if(count == Linge.Count)
        {
            list.isOn = true;
            FindObjectOfType<PlayerController>().jeu = false;
            Destroy(FindObjectOfType<MJ_MovementMain>().gameObject);
            Destroy(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Linge")
        {
            Destroy(collision.GetComponent<BoxCollider2D>());
            count++;
        }
    }
}
