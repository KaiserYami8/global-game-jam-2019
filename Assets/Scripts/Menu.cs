﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Menu : MonoBehaviour {

    [SerializeField]
    Toggle screenToggle;

    [SerializeField]
    Toggle musicToggle;

    [SerializeField]
    Slider Volume;

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void FullScreen()
    {
        Screen.fullScreen = screenToggle.isOn;
    }

    public void Mute()
    {
        /*foreach (GameObject Speaker in GameObject.FindGameObjectsWithTag("Sound"))
        {
            for (int i = 0; i < Speaker.GetComponents<AudioSource>().Length; i++)
            {
                Speaker.GetComponents<AudioSource>()[i].mute = !Speaker.GetComponents<AudioSource>()[i].mute;
            }
        }*/

        AudioListener.pause = !AudioListener.pause;
    }

    public void VolumeMusic()
    {
        AudioListener.volume = Volume.value;
    }

}
