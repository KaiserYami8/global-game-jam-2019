﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public Text timer;
    public float timeRemaining;

    string gameState;

	void Start () {
        gameState = "Waiting";
	}
	
	void Update () {
        if (gameState == "Ingame")
            timeRemaining = Mathf.Clamp(timeRemaining - Time.deltaTime, 0, 99.99f);
        if(timeRemaining <=0)
        {
            SceneManager.LoadScene(0);
        }

        string str = Mathf.Floor(timeRemaining).ToString("00") + " : " + ((timeRemaining - Mathf.Floor(timeRemaining)) * 99).ToString("00");
        timer.text = str;

        timer.fontSize = (int)Mathf.Clamp((250 - timeRemaining * 10), 32, 250);
        timer.color = new Color(Mathf.Clamp((250 - timeRemaining * 10), 0, 250) / 250, 0, 0);
	}

    public void EndCine()
    {
        FindObjectOfType<CameraController>().clamp -= 2;
        FindObjectOfType<PlayerController>().GetComponent<PlayerController>().enabled = true;

        GameObject.FindGameObjectWithTag("HUD").GetComponent<CanvasGroup>().alpha = 1;

        gameState = "Ingame";
    }
}
