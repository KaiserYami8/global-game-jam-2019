﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MJ_Poulet : MonoBehaviour {

    [SerializeField]
    string[] InputName;

    [SerializeField]
    int nbInput;

    [SerializeField]
    GameObject Poulet;

    [SerializeField]
    int Count;

    [SerializeField]
    Text InputDm;

    [SerializeField]
    string[] State;

    int NbState = 0;

    [SerializeField]
    GameObject[] Consigne;

    [SerializeField]
    Toggle list;

    // Use this for initialization
    void Start ()
    {
        InvokeRepeating("RandomInput", 0, 2);
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));


        if (State[NbState] == "Pousser")
        {
            if (Input.GetButtonDown(InputName[nbInput]))
            {
                
                Count++;
            }

            if (Count == 15)
            {
                NbState++;
                CancelInvoke("RandomInput");
                for (int i = 0; i < Consigne.Length; i++)
                {
                    Consigne[i].SetActive(false);
                }
                Consigne[2].SetActive(true);
            }
        }

        if (State[NbState] == "Rentrer")
        {
            if (input.x >= 0.9f)
            {
                NbState++;
            }
        }

        if (State[NbState] == "Win")
        {
            list.isOn = true;
            for (int i = 0; i < Consigne.Length; i++)
            {
                Consigne[i].SetActive(false);
            }
            FindObjectOfType<PlayerController>().jeu = false;
            Destroy(this);
        }

    }

    void RandomInput()
    {
        nbInput = Random.Range(0, InputName.Length);
        if(InputName[nbInput] == "Fire1")
        {
            Consigne[0].SetActive(true);
        }
        else
            Consigne[1].SetActive(true);
    }
}
