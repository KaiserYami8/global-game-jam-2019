﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MJ_NettoyageCaca : MonoBehaviour {

    [SerializeField]
    List<GameObject> Salete;

    [SerializeField]
    GameObject Consigne;

    [SerializeField]
    Toggle list;

    // Use this for initialization
    void Start () {

        foreach(GameObject sale in GameObject.FindGameObjectsWithTag("Salete"))
        {
            Salete.Add(sale);
        }
		
	}
	
	// Update is called once per frame
	void Update () {

        if(Salete.Count ==0)
        {
            list.isOn = true;
            Consigne.SetActive(false);
            FindObjectOfType<PlayerController>().jeu = false;
            Destroy(FindObjectOfType<MJ_MovementMain>().gameObject);
            Destroy(this);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Salete")
        {
            Salete.Remove(collision.gameObject);
            Destroy(collision.gameObject);
        }
    }
}
