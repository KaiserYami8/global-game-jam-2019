﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MJ_NePik : MonoBehaviour {

    [SerializeField]
    GameObject Piqure;

	public void NewPiqure()
    {
        Piqure = Instantiate(Piqure, transform.position, transform.rotation, transform);
        Piqure.GetComponent<MJ_SinMovement>().enabled = true;
    }
}
