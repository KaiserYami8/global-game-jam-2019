﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class DogeBehavior : MonoBehaviour {

    public GameObject txt, poop;
    public string[] sentences;

    public float timer, poopTimer;
    
	void Start () {
        timer = Random.value * .4f + .1f;
        poopTimer = Random.value * 10 + 20;
	}
	
	void Update () {
        timer -= Time.deltaTime;
        poopTimer -= Time.deltaTime;

        if (timer <= 0)
        {
            var go = Instantiate(txt, transform.position, Quaternion.identity);
            go.transform.GetChild(0).GetComponent<Text>().text = sentences[Random.Range(0, sentences.Length)];
            go.transform.GetChild(0).GetComponent<Text>().color = Color.HSVToRGB(Random.value, 1, 1);

            go.transform.position += new Vector3((Random.value - .2f) * 2, (Random.value - .2f) * 2, 0);

            go.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.value - .5f, Random.value - .5f) * 50);

            timer = Random.value * .4f + .1f;
        }

        if (poopTimer <= 0)
        {
            transform.GetChild(0).GetComponent<Animator>().SetTrigger("Poop");
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<PapyBehavior>().waitingTime = Random.value * 3 + 2;

            Instantiate(poop, transform.position, Quaternion.identity);

            poopTimer = Random.value * 10 + 20;
        }
	}

    public void Test()
    {

    }
}
