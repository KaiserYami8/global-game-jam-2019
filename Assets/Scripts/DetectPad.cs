﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectPad : MonoBehaviour {

    [SerializeField]
    int Etape;

    [SerializeField]
    int Tour;

    [SerializeField]
    GameObject Consigne;

    [SerializeField]
    Toggle list;

    private void Start()
    {
        Consigne.SetActive(true);
    }

    // Update is called once per frame
    void Update ()
    {

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (Etape == 0)
        {
            if (input.y >= 0.9f)
            {
                Debug.Log("en haut");
                Etape++;
            }
        }

        if (Etape == 1)
        {
            if (input.x >= 0.9f)
            {
                Debug.Log("Droit");
                Etape++;
            }
        }

        if (Etape == 2)
        {
            if (input.y <= -0.9f)
            {
                Debug.Log("Bas");
                Etape++;
            }
        }

        if (Etape == 3)
        {
            if (input.x <= -0.9f)
            {
                Debug.Log("Gauche");
                Etape++;
            }
        }

        if(Etape == 4)
        {
            Etape =1;
            Tour++;
        }

        if(Tour >= 10)
        {
            list.isOn = true;
            Consigne.SetActive(false);
            FindObjectOfType<PlayerController>().jeu = false;
            Destroy(this);
        }
    }
}
