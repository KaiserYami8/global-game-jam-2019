﻿using UnityEngine;

public class BabyBed : MonoBehaviour {

    public GameObject babyalone;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" && GetComponent<Animator>().GetBool("IsHere"))
        {
            Debug.Log(GetComponent<Rigidbody2D>().velocity.magnitude);
            
            if (GetComponent<Rigidbody2D>().velocity.magnitude > 6)
            {
                GetComponent<Animator>().SetBool("IsHere", false);
                var go = Instantiate(babyalone, transform.position, Quaternion.identity);

                go.GetComponent<ExplosionForce2D>().StartExplosion(transform.position);
            }
        }
    }

    public void RebuildBaby()
    {
        GetComponent<Animator>().SetBool("IsHere", true);
    }
}