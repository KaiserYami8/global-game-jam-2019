﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed;

    public Material classic, highlight;

    public GameObject interactable, activable, equiped, ObjectJeu;
    bool isEquiped;

    [SerializeField]
    public bool jeu;

    [SerializeField]
    GameObject Paper;

	void FixedUpdate () {

        if (!jeu)
        {
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            GetComponent<Rigidbody2D>().AddForce(input * speed);

            GetComponent<Animator>().SetFloat("Horizontal", input.x);
            GetComponent<Animator>().SetFloat("Vertical", input.y);
            GetComponent<Animator>().SetFloat("Magnitude", input.magnitude);


            if (Input.GetButtonDown("Fire1"))
            {
                GetComponent<Rigidbody2D>().AddForce(input * speed * 30);
            }

        }

        if (Input.GetButtonDown("Fire2"))
        {
            if(ObjectJeu != null)
            {
                ObjectJeu.GetComponent<Lancement_MJ>().JeuActive();
            }

            if (isEquiped)
            {
                if (activable != null)
                {
                    Debug.Log(equiped + " " + activable.GetComponent<TestReceiveItem>().target);
                    if (equiped.transform.GetChild(0).tag == activable.GetComponent<TestReceiveItem>().target)
                    {
                        activable.GetComponent<TestReceiveItem>().Test();
                        Destroy(equiped.gameObject);
                    }
                    else
                        Destroy(equiped.gameObject);
                }
                equiped = null;
                isEquiped = false;
                GetComponent<HingeJoint2D>().enabled = false;
                GetComponent<HingeJoint2D>().connectedBody = null;
            }
            else
            {
                if (interactable != null)
                {
                    equiped = interactable;
                    isEquiped = true;
                    GetComponent<HingeJoint2D>().enabled = true;
                    GetComponent<HingeJoint2D>().connectedBody = interactable.GetComponent<Rigidbody2D>();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Interactable")
        {
            if (interactable != null)
                interactable.GetComponent<SpriteRenderer>().material = classic;

            collision.GetComponent<SpriteRenderer>().material = highlight;
            interactable = collision.gameObject;
        }

        if (collision.gameObject.tag == "Activable")
        {
            if (activable != null)
                activable.GetComponent<SpriteRenderer>().material = classic;

            collision.GetComponent<SpriteRenderer>().material = highlight;
            activable = collision.gameObject;
        }

        if (collision.gameObject.tag == "Jeu")
        {
            collision.GetComponent<SpriteRenderer>().material = highlight;
            ObjectJeu = collision.gameObject;
        }

        if (collision.gameObject.tag == "Paper")
        {
            Paper.GetComponent<Animator>().SetBool("Close", false);
            Paper.GetComponent<Animator>().SetBool("Idle", false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Interactable")
        {
            collision.GetComponent<SpriteRenderer>().material = classic;
            interactable = null;
        }

        if (collision.gameObject.tag == "Activable")
        {
            collision.GetComponent<SpriteRenderer>().material = classic;
            activable = null;
        }

        if (collision.gameObject.tag == "Jeu")
        {
            collision.GetComponent<SpriteRenderer>().material = classic;
            ObjectJeu = null;
        }
        if (collision.gameObject.tag == "Paper")
        {
            Paper.GetComponent<Animator>().SetBool("Idle", true);
            Paper.GetComponent<Animator>().SetBool("Close", true);
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<SpriteRenderer>() != null)
        {
            if (transform.position.y < collision.transform.position.y)
            {
                GetComponent<SpriteRenderer>().sortingOrder = collision.GetComponent<SpriteRenderer>().sortingOrder + 1;
            }
            else
            {
                GetComponent<SpriteRenderer>().sortingOrder = collision.GetComponent<SpriteRenderer>().sortingOrder - 1;
            }
        }
    }
}