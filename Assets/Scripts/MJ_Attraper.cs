﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MJ_Attraper : MonoBehaviour
{

    public bool attrape = false;

    [SerializeField]
    GameObject Target;

    [SerializeField]
    bool Restart;

    [SerializeField]
    GameObject Start;

    [SerializeField]
    Text Interraction;

    [SerializeField]
    GameObject Consigne;

    [SerializeField]
    Toggle list;


    public void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            if(attrape)
            {
                list.isOn = true;
                Consigne.SetActive(false);
                FindObjectOfType<PlayerController>().jeu = false;
                Destroy(this);
            }
        }

        float pixelCam =  (Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height, 0)).y)*-1;

        if (Restart)
        {
            if(Target.transform.position.y < pixelCam)
            {
                if(attrape)
                {
                    return;
                }
                else
                {
                    Target.transform.position = Start.transform.position;
                    Target.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                }
            }
        }  
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == Target)
        {
            attrape = true;
            Consigne.SetActive(true);
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    { 
        if (collision.gameObject == Target)
        {
            attrape = false;
            Consigne.SetActive(false);
        }
    }
}
