﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PasserObjet : MonoBehaviour {

    [SerializeField]
    string Ramaser;

    [SerializeField]
    List<GameObject> Object;

    [SerializeField]
    Toggle list;

	// Use this for initialization
	void Start ()
    {
		foreach(GameObject GO in GameObject.FindGameObjectsWithTag(Ramaser))
        {
            Object.Add(GO);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = transform.parent.position;
        
        if(Object.Count == 0)
        {
            Debug.Log("Y a plus " + Ramaser);
            list.isOn = true;
            Destroy(this);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
{
        if(collision.gameObject.tag == Ramaser)
        {
            Debug.Log("Collision");
            Object.Remove(collision.gameObject);
            Destroy(collision.gameObject);
        }
    }

}
