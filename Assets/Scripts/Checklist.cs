﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checklist : MonoBehaviour {

    [SerializeField]
    Camera cam;

    [SerializeField]
    GameObject Paper;

	// Use this for initialization
	void Start () {
		
	}

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if(hit.transform == transform)
                {
                    Paper.GetComponent<Animator>().SetBool("Close", false);
                    Paper.GetComponent<Animator>().SetBool("Idle", false);
                }
            }
        }

        if(Paper.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))
        {
            Paper.GetComponent<Animator>().SetBool("Idle", true);
            Paper.GetComponent<Animator>().SetBool("Close", true);
        }
    }
}
