﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestReceiveItem : MonoBehaviour {

    public string target;

    [SerializeField]
    Toggle list;

    void Start () {
		
	}
	
	void Update () {
		
	}

    public void Test()
    {
        if (target == "Baby")
        {
            GetComponent<BabyBed>().RebuildBaby();
        }

        if (target == "Journal")
        {
            list.isOn = true;
        }
    }
}
