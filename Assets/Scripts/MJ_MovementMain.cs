﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MJ_MovementMain : MonoBehaviour {

    [SerializeField]
    int speed;
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        GetComponent<Rigidbody2D>().AddForce(input * speed);
    }
}
