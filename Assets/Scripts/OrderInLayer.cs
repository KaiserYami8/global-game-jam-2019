﻿using UnityEngine;

public class OrderInLayer : MonoBehaviour {

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<SpriteRenderer>() != null)
        {
            if (transform.position.y < collision.transform.position.y)
            {
                GetComponent<SpriteRenderer>().sortingOrder = collision.GetComponent<SpriteRenderer>().sortingOrder + 1;
            }
            else
            {
                GetComponent<SpriteRenderer>().sortingOrder = collision.GetComponent<SpriteRenderer>().sortingOrder - 1;
            }
        }
    }
}
