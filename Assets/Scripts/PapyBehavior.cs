﻿using UnityEngine;
using UnityEngine.AI;

public class PapyBehavior : MonoBehaviour {

    public GameObject[] targets;

    public float waitingTime;
    
	void Start () {
        GetComponent<Player>().GoToTarget();
    }
	
	void Update () {
		if (Vector3.Distance(transform.position, GetComponent<Player>().Target.transform.position) <= 1f && waitingTime < 0)
        {
            GetComponent<NavMeshAgent>().enabled = false;
            waitingTime = Random.value * 3 + 2;
        }
        waitingTime -= Time.deltaTime;
        if (waitingTime <= 0 && !GetComponent<NavMeshAgent>().enabled)
        {
            GetComponent<NavMeshAgent>().enabled = true;
            GetComponent<Player>().Target = targets[Random.Range(0, targets.Length)];
            GetComponent<Player>().GoToTarget();
        }
	}
}
