﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    public float clamp;

    void Update()
    {
        var x = Mathf.Clamp(player.transform.position.x, -clamp, clamp);
        var y = Mathf.Clamp(player.transform.position.y, -clamp, clamp);
        //transform.position = new Vector3(x, y, -10);
        transform.position = Vector3.Lerp(transform.position, new Vector3(x, y, -10), .8f);

    }

    //public Transform FollowTarget;
    //public Vector3 TargetOffset;
    //public float MoveSpeed = 2f;

    //private Transform _myTransform;

    //private void Start()
    //{
    //    // Cache camera transform
    //    _myTransform = transform;
    //}

    //public void SetTarget(Transform aTransform)
    //{
    //    FollowTarget = aTransform;
    //}

    //private void LateUpdate()
    //{
    //    if (FollowTarget != null)
    //        _myTransform.position = Vector3.Lerp(_myTransform.position, FollowTarget.position + TargetOffset, MoveSpeed * Time.deltaTime);
    //}
}
