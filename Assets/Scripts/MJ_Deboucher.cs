﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MJ_Deboucher : MonoBehaviour {

    [SerializeField]
    bool Haut;

    [SerializeField]
    int Tour;

    [SerializeField]
    GameObject Consigne;

    [SerializeField]
    Toggle list;


    private void Start()
    {
        Consigne.SetActive(true);
    }

    // Update is called once per frame
    void Update ()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (Haut)
        {
            if (input.y >= 0.9f)
            {
                Haut = false;
                Tour++;
            }
        }
        if (!Haut)
        {
            if (input.y <= -0.9f)
            {
                Haut = true;
                Tour++;
            }
        }

        if(Tour > 15)
        {
            list.isOn = true;
            Consigne.SetActive(false);
            FindObjectOfType<PlayerController>().jeu = false;
            Destroy(this);
        }
    }
}
