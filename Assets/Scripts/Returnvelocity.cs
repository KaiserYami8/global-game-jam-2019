﻿using UnityEngine;
using UnityEngine.AI;

public class Returnvelocity : MonoBehaviour {

    Vector2 oldPos;

	void Update () {


        //Vector2 input = GetComponent<Rigidbody2D>().velocity;
        //Vector2 position = transform.position;
        //Vector2 input = (position - oldPos) * 1000;

        Vector2 input = GetComponent<NavMeshAgent>().velocity;

        var anim = transform.GetChild(0).GetComponent<Animator>();
        anim.SetFloat("Horizontal", input.x);
        anim.SetFloat("Vertical", input.y);
        anim.SetFloat("Magnitude", input.magnitude);
    }

    private void LateUpdate()
    {
        oldPos = transform.position;
    }
}